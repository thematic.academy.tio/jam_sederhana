import React, { Component } from 'react';
import DateClass from './DateClass';
import DateFunction from './DateFunction';
import '../asset/bootstrap.css';

class Clock extends Component {
    constructor() {
        super();
        this.state = {
            datetime: new Date(),
            dateIsFunction: false,
        };
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.timerID = setInterval(() => {
            this.setState({ datetime: new Date() });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    handleClick() {
        this.setState((state) => ({
            dateIsFunction: !state.dateIsFunction,
        }));
    }
    render() {
        let dateComponent;
        if (this.state.dateIsFunction) {
            dateComponent = <DateFunction />;
        } else {
            dateComponent = <DateClass date={this.state.datetime} />;
        }
        return (
            <div className="container-fluid">
                {dateComponent}
                {this.state.datetime.toLocaleTimeString()}
                <br />
                <p>Ubah waktu komponen ke:</p>
                <button
                    onClick={this.handleClick}
                    className="btn button-big btn-danger"
                >
                    {this.state.dateIsFunction ? 'CLASS' : 'FUNCTION'}
                </button>
            </div>
        );
    }
}

export default Clock;
