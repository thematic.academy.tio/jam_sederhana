import React, { useState, useEffect } from 'react';

export default function DateFunction() {
    const [date, setDate] = useState(new Date());

    useEffect(() => {
        const timerID = setInterval(() => {
            setDate(new Date());
        }, 1000);
        return () => {
            clearInterval(timerID);
        };
    }, []);

    return (
        <div>
            <h1>Waktu yang dibuat dengan functional komponen dengan Hook</h1>
            <p>{date.toLocaleDateString()}</p>
        </div>
    );
}
