import './App.css';
import Clock from './Komponen/Clock';
function App() {
    return (
        <div className="App">
            <header className="App-header">
                <Clock />
            </header>
        </div>
    );
}

export default App;
